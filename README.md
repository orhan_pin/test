# Training

**2/8/2021:**

- Customized Users model (AbstractUser): added new fields. [**SS**](https://s17.picofile.com/file/8424484392/1.png)

- Created forms using forms.Form for loging in and signing up in the site(also added custom widgets with extra attributes). Also worked with "clean" functions in forms.Form class. [**Login**](https://s16.picofile.com/file/8424484600/2.png)|[**SignUp**](https://s17.picofile.com/file/8424484668/3.png)

- Used bootstrap classes a little bit(floating forms).
