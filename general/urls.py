from django.urls import path
from .views import login_page, signup_page, home_page

urlpatterns = [
    path('', home_page, name='home'),
    path('login', login_page, name='login'),
    path('signup', signup_page, name='signup'),
]
