from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from .models import CustomUser
from django import forms


class UserCreationForm(forms.ModelForm):
    age = forms.IntegerField()

    class Meta:
        model = CustomUser
        fields = '__all__'


class CustomUserAdmin(UserAdmin):
    add_form = UserCreationForm
    list_display = ['username', 'email', 'age', 'is_staff']
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('age',)}),
    )


admin.site.register(CustomUser, CustomUserAdmin)
