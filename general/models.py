from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    username = models.CharField(unique=True, max_length=15)
    email = models.EmailField()
    age = models.IntegerField()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'age']

    def __str__(self):
        return self.username
