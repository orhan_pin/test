from django.contrib.auth import authenticate, login
from django.contrib.auth import get_user_model
from django.shortcuts import render, redirect

from .forms import SignUpForm, LoginForm


def home_page(request):
    context = {
        "title": "صفحه ی اصلی",
        "content": "به آموزش جنگو خوش آمدید"
    }

    return render(request, "base.html", context)


def login_page(request):
    form = LoginForm(request.POST or None)
    context = {
        "form": form
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            context["form"] = LoginForm()
            return redirect('home')

    return render(request, "login.html", context)


def signup_page(request):
    form = SignUpForm(request.POST or None)
    context = {
        'form': form
    }
    if form.is_valid():
        data = form.cleaned_data
        user = get_user_model()
        new_user = user.objects.create_user(username=data.get('username'), password=data.get('password'),
                                            email=data.get('email'),
                                            age=data.get('age'))
        new_user.save()
        context['form'] = SignUpForm()
    return render(request, 'signup.html', context)
